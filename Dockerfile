FROM python:3-bullseye

RUN apt update -y
RUN apt install -y texlive-xetex texlive-lang-arabic texlive-lang-other culmus fonts-linuxlibertine
RUN sed -i'.bak' 's/$/ contrib/' /etc/apt/sources.list
RUN apt-get update; apt-get install -y ttf-mscorefonts-installer fontconfig


RUN pip install --upgrade pip
RUN pip install scrapy pymongo jinja2 python-hebrew-numbers


RUN mkdir -p /bookscraper

RUN adduser --disabled-password --gecos '' myuser

RUN chown -R myuser:myuser /bookscraper

USER myuser



# Setup working directory


WORKDIR /bookscraper



