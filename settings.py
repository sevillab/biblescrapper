from typing import OrderedDict


BOOKS_HEBREW = OrderedDict()
BOOKS_GREEK = OrderedDict()

books_torah = OrderedDict([
    ('genesis', 'בְּרֵאשִׁית'),
    ('exodus', 'שְׁמוֹת'),
    ('leviticus', 'וַיִּקְרָא'),
    ('numbers', 'בַּמִּדְבָּר'),
    ('deuteronomy', 'דְּבָרִים')
])

books_prophets = OrderedDict([
    ('joshua', 'יְהוֹשֻעַ'),
    ('judges', 'שֹׁפְטִים'),
    ('1_samuel', 'שְׁמוּאֵל א'),
    ('2_samuel', 'שְׁמוּאֵל ב'),
    ('1_kings', 'מְלָכִים א'),
    ('2_kings', 'מְלָכִים ב'),
    ('isaiah', 'יְשַׁעְיָהוּ'),
    ('jeremiah', 'יִרְמְיָהוּ'),
    ('ezekiel', 'יְחֶזְקֵאל'),
    ('hosea', 'הוֹשֵׁעַ'),
    ('joel', 'יוֹאֵל'),
    ('amos', 'עָמוֹס'),
    ('obadiah', 'עֹבַדְיָה'),
    ('jonah', 'יוֹנָה'),
    ('micah', 'מִיכָה'),
    ('nahum', 'נַחוּם'),
    ('habakkuk', 'חֲבַקּוּק'),
    ('zephaniah', 'צְפַנְיָה'),
    ('haggai', 'חַגַּי'),
    ('zechariah', 'זְכַרְיָה'),
    ('malachi', 'מַלְאָכִי'),
])

books_writings = OrderedDict([
    ('psalms', 'תְהִלִּים'),
    ('proverbs', 'מִשְׁלֵי'),
    ('job', 'אִיּוֹב'),
    ('songs', 'שִׁיר הַשִּׁירִים'),
    ('ruth', 'רוּת'),
    ('lamentations', 'אֵיכָה'),
    ('ecclesiastes', 'קֹהֶלֶת'),
    ('esther', 'אֶסְתֵר'),
    ('daniel', 'דָּנִיֵּאל'),
    ('ezra', 'עֶזְרָא'),
    ('nehemiah', 'נְחֶמְיָה'),
    ('1_chronicles', 'דִּבְרֵי הַיָּמִים א'),
    ('2_chronicles', 'דִּבְרֵי הַיָּמִים ב'),

])

books_nt = OrderedDict([
    ('matthew', 'Κατά Ματθαίον'),
    ('mark', 'Κατά Μάρκον'),
    ('luke', 'Κατά Λουκάν'),
    ('john', 'Κατά Ιωάννην'),
    ('acts', 'Πράξεις'),
    ('romans', 'Προς Ρωμαίους'),
    ('1_corinthians', 'Προς Κορινθίους Αʹ'),
    ('2_corinthians', 'Προς Κορινθίους Βʹ'),
    ('galatians', 'Προς Γαλάτας'),
    ('ephesians', 'Προς Εφεσίους'),
    ('philippians', 'Προς Φιλιππησίους'),
    ('colossians', 'Προς Κολοσσαείς'),
    ('1_thessalonians', 'Προς Θεσσαλονικείς Αʹ'),
    ('2_thessalonians', 'Προς Θεσσαλονικείς Βʹ'),
    ('1_timothy', 'Προς Τιμόθεον Αʹ'),
    ('2_timothy', 'Προς Τιμόθεον Βʹ'),
    ('titus', 'Προς Τίτον'),
    ('philemon', 'Προς Φιλήμονα'),
    ('hebrews', 'Προς Εβραίους'),
    ('james', 'Ιακώβου'),
    ('1_peter', 'Πέτρου Αʹ'),
    ('2_peter', 'Πέτρου Βʹ'),
    ('1_john', 'Ιωάννου Αʹ'),
    ('2_john', 'Ιωάννου Βʹ'),
    ('3_john', 'Ιωάννου Γʹ'),
    ('jude', 'Ιούδα'),
    ('revelation', 'Αποκάλυψις Ιωάννου'),
])


# Select book title of the book to generate

TITLE = 'תּוֹרָה Torah'
# TITLE = "נְבִיאִים Nevi'im"
# TITLE = 'כְּתוּבִים Kethuvim'
# TITLE = 'תַּנַ״ךְ Tanakh'
#TITLE = 'Καινή Διαθήκη'

if 'Torah' in TITLE:
    BOOKS_HEBREW.update(books_torah)
if 'Nevi' in TITLE:
    BOOKS_HEBREW.update(books_prophets)
if 'Kethuvim' in TITLE:
    BOOKS_HEBREW.update(books_writings)

if 'Tanakh' in TITLE:
    BOOKS_HEBREW.update(books_torah)
    BOOKS_HEBREW.update(books_prophets)
    BOOKS_HEBREW.update(books_writings)

if 'Καινή Διαθήκη' == TITLE:
    BOOKS_GREEK.update(books_nt)
