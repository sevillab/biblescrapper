Generate bible transliteration book with data scraped from https://biblehub.com, produces a Latex pdf for A4 paper for printing or A6 paper for kindle-like reading. Scraped data is stored in a Mongo database, a XeLateX file is generated using Jinja2 templating and compiled with xetex.

1. Modify settings.py. Uncomment the title of the section you want to generate
```
# TITLE = 'תּוֹרָה Torah'
# TITLE = "נְבִיאִים Nevi'im"
# TITLE = 'כְּתוּבִים Kethuvim'
# TITLE = 'תַּנַ״ךְ Tanakh'
# TITLE = 'Καινή Διαθήκη'
```
2. Start containers
```
make setup
```
3. Scrap the data, You can access Mongo Express at http://localhost:8081 with the credentials in the _env_ file.
```
make scrap
```
4. Generate PDF according to the title you chose and the format, it will output a file 'texbook.pdf'
```
make hebrew-pdf-book
make hebrew-pdf-kindle
make greek-pdf-book
make greek-pdf-kindle
```
