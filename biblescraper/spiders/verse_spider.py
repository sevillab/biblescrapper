import json
import scrapy
from settings import BOOKS_GREEK, BOOKS_HEBREW


EXCLUDE_BOOK_LINK = [
    'vmenus/', 'vmm/', 'aaaNT/'
]


class VerseSpider(scrapy.Spider):
    name = "verses"

    def start_requests(self):
        urls = [
            'https://biblehub.com/text/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.follow_books)

    def follow_books(self, response):
        tr_book_list = response.css('body > table > tr')
        for tr_book in tr_book_list:
            tds_book = tr_book.css('td')
            if len(tds_book) != 5:
                continue
            size = tds_book.css('td::text')[3].get().strip()
            name_link = tds_book.css('td > a::text').get()
            link = tds_book.css('td > a::attr(href)')
            book = name_link.replace('/', '')
            if book not in BOOKS_HEBREW.keys() and book not in BOOKS_GREEK.keys():
                continue
            if size == '-' and name_link.endswith('/') and name_link not in EXCLUDE_BOOK_LINK:
                yield response.follow(link.get(), meta={'book': book}, callback=self.follow_verses)

    def follow_verses(self, response):
        tr_verse_list = response.css('body > table > tr')
        for tr_verse in tr_verse_list:
            tds_verse = tr_verse.css('td')
            if len(tds_verse) != 5:
                continue
            size = tds_verse.css('td::text')[3].get().strip()
            name = tds_verse.css('td > a::text').get()
            link = tds_verse.css('td > a::attr(href)')
            if 'K' in size and '-' in name:
                chapter, verse = name.replace('.htm', '').split('-')
                yield response.follow(link.get(), meta={
                    'book': response.meta['book'], 'chapter': int(chapter), 'verse': int(verse)},
                    callback=self.parse, priority=1)

    def parse(self, response, **kwargs):
        words = []
        verse = {
            'book': response.meta['book'],
            'chapter': response.meta['chapter'],
            'verse': response.meta['verse'],
        }
        for word_tr in response.css('table.maintext > tr'):
            if word_tr.css('td.top'):
                continue
            strong = word_tr.css('td.strongsnt >span > a::text').get()
            hebrew = word_tr.css('td.hebrew2::text').get()
            hebrew_translit = word_tr.css('td.hebrew2 >span > a::text').get()
            greek = word_tr.css('td.greek2::text').get()
            greek_translit = word_tr.css('td.greek2 >span > a::text').get()
            english = word_tr.css('td.eng::text').get()
            morph = word_tr.css('td.pos > span > a::text').get()
            words.append({
                'strong': strong,
                'original': hebrew or greek,
                'translit': hebrew_translit or greek_translit,
                'english': english,
                'morph': morph
            })
        verse['words'] = words
        print(f'{verse["book"]} {verse["chapter"]}:{verse["verse"]}')
        yield verse
