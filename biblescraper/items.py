# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class BookItem(scrapy.Item):
    # define the fields for your item here like:
    name = scrapy.Field()

class ChapterItem(scrapy.Item):
    # define the fields for your item here like:
    number = scrapy.Field()
    verses = scrapy.Field()

class VerseItem(scrapy.Item):
    # define the fields for your item here like:
    number = scrapy.Field()
    words = scrapy.Field()

class WordItem(scrapy.Item):
    # define the fields for your item here like:
    original = scrapy.Field()
    translation = scrapy.Field()
    strong = scrapy.Field()
    morphology = scrapy.Field()
    morphology_abbr = scrapy.Field()
    

class BiblescraperItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass
