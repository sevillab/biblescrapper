# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import pymongo
import os


def clear_orig(orig):
    cleared = orig
    for char in ['[', ']', '{', '}']:
        cleared = cleared.replace(char, '')
    return cleared


def clear_translit(translit):
    cleared = translit
    for char in ['[', ']', '(', ')', '{', '}']:
        cleared = cleared.replace(char, '')
    return cleared


def clear_english(english):
    cleared = english
    for char, clear in [('{', '['), ('}', ']'), ('_', ' ')]:
        cleared = cleared.replace(char, clear)
    return cleared


class BiblescraperPipeline:
    def __init__(self):
        self.conn = pymongo.MongoClient(
            host=os.environ.get('MONGO_HOST', default='localhost'),
            port=27017,
            username=os.environ.get('MONGO_INITDB_ROOT_USERNAME', default='root'),
            password=os.environ.get('MONGO_INITDB_ROOT_PASSWORD', default='password')
        )
        self.db = self.conn['bible']

    def process_item(self, item, spider):
        if spider.name == 'verses':
            for word in item['words']:
                if word.get('strong') is None:
                    word['strong'] = ''
                else:
                    word['strong'] = f'[{word["strong"]}]'
                if word.get('original') is None:
                    word['original'] = ''
                else:
                    word['original'] = clear_orig(word['original'])

                if word.get('translit') is None:
                    word['translit'] = ''
                else:
                    word['translit'] = clear_translit(word['translit'])
                if word.get('english') is None:
                    word['english'] = ''
                else:
                    word['english'] = clear_english(word['english'])
                if word.get('morph') is None:
                    word['morph'] = ''

            book = item.pop('book').split('_')
            book_name = f'{book[1]}_{book[0]}' if len(book) == 2 else book[0]
            collection = self.db[book_name]
            collection.insert_one(item)
            # if not collection.find_one({'chapter': item['verse'], 'verse': item['verse']}):
