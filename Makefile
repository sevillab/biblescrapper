setup:
	docker-compose up -d

scrap:
	docker-compose exec app scrapy crawl verses

hebrew-pdf-book:
	docker-compose exec app python template.py hebrew template_paper_h.tex
	docker-compose exec app xelatex texbook.tex

hebrew-pdf-kindle:
	docker-compose exec app python template.py hebrew template_kindle_h.tex
	docker-compose exec app xelatex texbook.tex

greek-pdf-book:
	docker-compose exec app python template.py greek template_paper_g.tex
	docker-compose exec app xelatex texbook.tex

greek-pdf-kindle:
	docker-compose exec app python template.py greek template_kindle_g.tex
	docker-compose exec app xelatex texbook.tex