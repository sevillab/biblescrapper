from settings import BOOKS_HEBREW, BOOKS_GREEK, TITLE
from itemadapter import ItemAdapter
import pymongo
from jinja2 import Template, FileSystemLoader, Environment
from hebrew_numbers import int_to_gematria
import os
import sys


if len(sys.argv) < 3:
    raise Warning('Enter the language (hebrew/greek) and name of the template file')
_, LANGUAGE, TEMPLATE_FILE = sys.argv

GREEK_NUMERAL_TABLE = (
    ("α", 1), ("β", 2), ("γ", 3),
    ("δ", 4), ("ε", 5), ("Ϝ", 6),
    ("ζ", 7), ("η", 8), ("θ", 9),
    ("ι", 10), ("κ", 20), ("λ", 30),
    ("μ", 40), ("ν", 50), ("ξ", 60),
    ("ο", 70), ("π", 80), ("ϙ", 90),
    ("ρ", 100), ("σ", 200), ("τ", 300),
    ("υ", 400), ("φ", 500), ("χ", 600),
    ("ψ", 700), ("ω", 800), ("ϡ", 900),
    ("α", 1000), ("β", 2000), ("γ", 3000),
    ("δ", 4000), ("ε", 5000), ("ϛ", 6000),
    ("ζ", 7000), ("η", 8000), ("θ", 9000)
)


class HTMLGenerator:
    def __init__(self):
        self.conn = pymongo.MongoClient(
            host=os.environ.get('MONGO_HOST', default='localhost'),
            port=27017,
            username=os.environ.get('MONGO_INITDB_ROOT_USERNAME', default='root'),
            password=os.environ.get('MONGO_INITDB_ROOT_PASSWORD', default='password')
        )
        self.db = self.conn['bible']


generator = HTMLGenerator()
books_to_generate = []

if LANGUAGE == 'greek':
    books_to_generate = BOOKS_GREEK

if LANGUAGE == 'hebrew':
    books_to_generate = BOOKS_HEBREW

book_collections = []
for book_name in books_to_generate:
    book = book_name.split('_')
    book_name_ = f'{book[1]}_{book[0]}' if len(book) == 2 else book[0]
    book_collection = generator.db[book_name]
    book = book_collection.find(sort=[('chapter', 1), ('verse', 1)])
    name = book_name.upper()
    book_collections.append([name, book])


def break_morph(value):
    return value.replace(' | ', '|')


def hebrew_number(number):
    try:
        return(int_to_gematria(number, gershayim=False))
    except:
        return '\\hfill'


def greek_number(number):
    try:
        display_numerals = []
        convert_to = sorted(GREEK_NUMERAL_TABLE, key=lambda e: e[1])[::-1]
        for numeral, value in convert_to:  # sort the list from largest to least
            count = number // value
            number -= count * value
            display_numerals.append(numeral * count)

        return ''.join(display_numerals)
    except:
        return '\\hfill'


def hebrew_book(book_eng):
    return BOOKS_HEBREW.get(book_eng.lower(), '')


def greek_book(book_eng):
    return BOOKS_GREEK.get(book_eng.lower(), '')


def check_translit_hebrew(word):
    if word['strong'] == '' and word['morph'] == 'Punc' and word['translit'] == 'p̄':
        return False
    if word['strong'] == '' and word['morph'] == '' and word['original'] == '':
        return False
    return True


def check_translit_greek(word):
    return True


def check_orig(word):
    if word['strong'] == '' and word['morph'] == '' and word['original'] == '':
        return False
    return True


loader = FileSystemLoader(searchpath="./")
template_env = Environment(
    block_start_string='\BLOCK{',
    block_end_string='}',
    variable_start_string='\VAR{',
    variable_end_string='}',
    comment_start_string='\#{',
    comment_end_string='}',
    line_statement_prefix='%%',
    line_comment_prefix='%#',
    trim_blocks=True,
    autoescape=False,
    loader=loader)

template_env.globals.update(break_morph=break_morph)

if LANGUAGE == 'hebrew':
    template_env.globals.update(lang_number=hebrew_number)
    template_env.globals.update(lang_book=hebrew_book)
    template_env.globals.update(check_translit=check_translit_hebrew)
if LANGUAGE == 'greek':
    template_env.globals.update(lang_number=greek_number)
    template_env.globals.update(lang_book=greek_book)
    template_env.globals.update(check_translit=check_translit_greek)
template_env.globals.update(check_orig=check_orig)

template = template_env.get_template(TEMPLATE_FILE)

with open('texbook.tex', 'w') as book:
    book.write(template.render(books=book_collections, title=TITLE))
